var myApp = {};
(function() {
  var app = function(options) {
    this.initialize(options);
  };
  app.prototype = {
    selectors: function() {
      this.clock  = document.querySelector('.digital');
      this.username = document.getElementById('username');
      this.getCurrentTimeButton = document.getElementById('getCurrentTime');
      this.serverTime = document.getElementById('serverTime');
      this.userTimezone = document.getElementById('userTimezone');
    },
    ajax: function(options, callback) {
      var request = new XMLHttpRequest();

      request.open(options.method, options.url, true);
      request.send();

      request.addEventListener('error', function(evt) {
        callback('error');
      });

      request.addEventListener('load', function(evt) {
        callback(JSON.parse(request.responseText));
      });
    },
    /**
     * Convert to two digits.
     *   - Example:
     *       + getTwoDigits(1) return 01
     *       + getTwoDigits(15) return 15
     */
    getTwoDigits: function(val) {
      if (val < 10) {
        return ('0' + val.toString());
      } else {
        return val.toString();
      }
    },
    /**
     * Socket listener for emit from the server.
     */
    socketsListen: function() {
      var self = this;

      this.socket.on('time', function(data) {
        self.showCurrentTime(data);
      });
    },
    /**
     * Tell the server to stop emit.
     */
    stopCurrentTimeSockets: function() {
      this.socket.emit('stopCurrentTime');
    },
    /**
     * Tell the server to start emit for a Timezone.
     */
    getCurrentTimeSockets: function(timeZone) {
      this.stopCurrentTimeSockets();
      this.socket.emit('getCurrentTime', timeZone);
    },
    /**
     * AJAX call that return current time for a Timezone.
     */
    getCurrentTimeAjax: function(timeZone, callback) {
      this.ajax({
        method: 'GET',
        url: '/currentTime/' + timeZone
      }, function(data) {
        callback(data);
      });
    },
    /**
     * AJAX call that return user timezone.
     */
    getUserTimeZone: function(id, callback) {
      var userId = parseInt(id);

      if ( (false === isNaN(userId)) || (userId >= -10 && userId <= 10) ) {
        this.ajax({
          method: 'GET',
          url: '/user/' + userId
        }, function(data) {
          callback(data);
        });
      } else {
        callback('error');
      }
    },
    /**
     * Simulate real time with long polling.
     */
    longPolling: function(timezone) {
      var self = this;

      clearInterval(this.longPollingInterval);

      this.longPollingInterval = setInterval(function() {
        self.getCurrentTimeAjax(timezone, self.showCurrentTime.bind(self));
      }, 1000);
    },
    /**
     * Render HTML with server data.
     */
    showCurrentTime: function(data) {
      var time = new Date(data.serverTime);

      var hours = this.getTwoDigits(time.getHours());
      var minutes = this.getTwoDigits(time.getMinutes());
      var seconds = this.getTwoDigits(time.getSeconds());

      var toClock = hours + ':' + minutes + ':' + seconds;

      this.clock.innerHTML = toClock;

      this.serverTime.innerHTML = '<strong>Server time:</strong> ' + data.originalServerTime;
    },
    /**
     * Get userID from input.
     */
    getInputData: function(e) {
      if (e.keyCode === 13 || e.type === 'click') {
        var self = this;
        var userId = this.username.value;

        this.getUserTimeZone(userId, function(data) {
          if (data === 'error') {
            alert('Incorrect ID');
            data = 0;
          }

          if (self.options.protocol === 'sockets') {
            self.getCurrentTimeSockets(data);
          }

          if (self.options.protocol === 'longPolling') {
            self.longPolling(data);
          }
        });
      }
    },
    /**
     * Start application.
     */
    initialize: function(options) {
      this.options = options;

      if (typeof(document) !== 'undefined') {
        this.selectors();

        if (this.getCurrentTimeButton) {
          this.getCurrentTimeButton.addEventListener('click', this.getInputData.bind(this));
        }

        if (this.username) {
          this.username.addEventListener('keypress', this.getInputData.bind(this));
        }
      }

      if (options.protocol === 'sockets') {
        this.socket = io.connect('http://localhost:4321');
        this.socketsListen();

        this.getCurrentTimeSockets(0);
      }

      if (options.protocol === 'longPolling') {
        this.longPolling(0);
      }
    }
  };

  myApp.client = app;
})();

/**
 * New app instance.
 * You can set protocol:
 *
 * Examples:
 *   - new myApp.client({ protocol: 'sockets' });
 *   - new myApp.client({ protocol: 'longPolling' });
 */
if (typeof(document) !== 'undefined') {
  var clientApp = new myApp.client({
    protocol: 'sockets'
  });
}