/**
 * Service that gives a JSON to
 * /currentTime/:timeZone
 */
var imports = {
  Q: require('q')
};

var Q = imports.Q;

(function () {
  var CurrentTimeService = function () {
  };

  CurrentTimeService.prototype = {
    getCurrentTime: function(timezone) {
      var dfd = Q.defer();

      setTimeout(function(){
        var data = {};
        var time = new Date();
        var setTimeZone = time.getHours() + parseInt(timezone);

        time.setHours(setTimeZone);

        data.serverTime = time.toString();
        data.originalServerTime = new Date().toString();

        dfd.resolve(data);
      }, Math.floor(Math.random() * 5));

      return dfd.promise;
    }
  };

  module.exports = CurrentTimeService;
})();