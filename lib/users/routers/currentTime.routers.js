/**
 * Listen for /currentTime/:timeZone
 * and respond with json like:
 *  {
 *    "serverTime": "Sun Jan 12 2014 21:55:06 GMT+0100 (CET)",
 *    "originalServerTime": "Sun Jan 12 2014 18:55:06 GMT+0100 (CET)"
 *  }
 */
var imports = {
  CurrentTimeService: require('../services/currentTimeService')
};
(function() {
  var currentTimeRouters = function(express) {
    this.initialize(express);
    this.currentTimeService = new imports.CurrentTimeService();
  };

  currentTimeRouters.prototype = {
    initialize: function(express) {
      express.get('/currentTime/:timeZone', this.currentTimeZone.bind(this));
    },
    currentTimeZone: function(req, res) {
      this.currentTimeService.getCurrentTime(req.params.timeZone)
      .then(function(result) {
        res.jsonp(result);
      })
      .fail(function() {
        res.end(404);
      });
    }
  };

  module.exports = currentTimeRouters;
})();