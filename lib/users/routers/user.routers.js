/**
 * Listen for /user/:userId
 * and respond with a value like: -8
 */
var imports = {
  UserService: require('../services/userService')
};
(function() {
  var userRouters = function(express) {
    this.initialize(express);
    this.userService = new imports.UserService();
  };

  userRouters.prototype = {
    initialize: function(express) {
      express.get('/user/:userId', this.userInfo.bind(this));
    },
    userInfo: function(req, res) {
      this.userService.getUserTimeZone(req.params.userId)
      .then(function(result) {
        res.send(200, ""+result);
      })
      .fail(function() {
        res.end(404);
      });
    }
  };

  module.exports = userRouters;
})();
