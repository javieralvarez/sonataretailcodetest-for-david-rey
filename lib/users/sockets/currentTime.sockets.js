/**
 * Listen to currentTime sockets events:
 *   - getCurrentTime: return the same JSON as /currentTime/:timeZone
 *   - stopCurrentTime: stop emiting events from the server.
 */
var imports = {
  CurrentTimeService: require('../services/currentTimeService')
};
(function() {
  var currentTimeSockets = function(socket) {
    this.socket = socket;
    this.initialize(this.socket);
    this.currentTimeService = new imports.CurrentTimeService();
  };

  currentTimeSockets.prototype = {
    initialize: function(socket) {
      socket.on('getCurrentTime', this.currentTimeZone.bind(this));
      socket.on('stopCurrentTime', this.stopCurrentTime.bind(this));
    },
    currentTimeZone: function(timeZone) {
      var self = this;

      this.currentTime = setInterval(function() {
        self.currentTimeService.getCurrentTime(timeZone)
        .then(function(result) {
          self.socket.emit('time', result);
        })
        .fail(function() {
          self.socket.emit('time', {errorMsg: 404});
        });
      }, 1000);
    },
    stopCurrentTime: function() {
      clearInterval(this.currentTime);
    }
  };

  module.exports = currentTimeSockets;
})();