module.exports = function(grunt) {

  grunt.initConfig({

    jshint: {
      options: {
        boss: true,
        curly: true,
        eqeqeq: true,
        eqnull: true,
        immed: true,
        indent: 2,
        latedef: true,
        laxcomma: true,
        newcap: true,
        noarg: true,
        node: true,
        sub: true,
        undef: true,
        globals: {
          window: true,
          document: true,
          XMLHttpRequest: true,
          alert: true,
          describe: true,
          beforeEach: true,
          afterEach: true,
          it: true,
          io: true
        },
        ignores: ['test/client/clientApp.test.js']
      },
      all: [
        'Gruntfile.js',
        'lib/**',
        'public/js/client.js',
        'test/**/*.js'
      ]
    },

    /**
     * Run server Unit Tests.
     */
    simplemocha: {
      options: {
        timeout: 3000,
        ignoreLeaks: false,
        ui: 'bdd',
        reporter: 'tap'
      },

      all: { src: 'test/server/**/*.js' }
    },

    /**
     * Run client Unit Tests.
     */
    mocha: {
      test: {
        src: ['test/**/*.html'],
        options: {
          reporter: 'Spec',
          run: true,
          timeout: 10000
        }
      },
    },

    watch: {
      options: {
        event: ['added', 'changed']
      },
      jshint: {
        files: [
          'Gruntfile.js',
          'lib/**',
          'public/js/client.js',
          'test/**/*.js'
        ],
        tasks: ['jshint']
      },
      mocha: {
        files: [
          'lib/**',
          'client/scripts/**',
        ],
        tasks: ['simplemocha']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-simple-mocha');
  grunt.loadNpmTasks('grunt-mocha');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['jshint', 'simplemocha', 'mocha']);
};