var global = {};
var imports = {
  express: require('express'),
  http: require('http'),
  socketio: require('socket.io'),
  UserRouters: require('./lib/users/routers/user.routers'),
  CurrentTimeRouters: require('./lib/users/routers/currentTime.routers'),
  CurrentTimeSockets: require('./lib/users/sockets/currentTime.sockets')
};

(function() {
  var app = function(options) {
    this.initialize(options);
  };
  app.prototype = {
    initialize: function(imports) {
      this.imports = imports;

      this.express = this.imports.express();

      this.express.use(this.imports.express.static(__dirname + '/public'));

      this.server = this.imports.http.createServer(this.express);

      this.routers();
    },
    routers: function() {
      this.userRouters = new this.imports.UserRouters(this.express);
      this.currentTimeRouters = new this.imports.CurrentTimeRouters(this.express);
    },
    /**
     * Imports sockets events
     */
    sockets: function(socket) {
      this.currentTimeSockets = new this.imports.CurrentTimeSockets(socket);

      socket.on('disconnect', function() {
        socket._events.stopCurrentTime();
      });
    },
    listen: function(port) {
      this.server.listen(port);

      this.io = this.imports.socketio.listen(this.server);
      this.io.sockets.on('connection', this.sockets.bind(this));
    }
  };
  global.server = app;

  /**
   * module.exports just for running Unit Tests.
   */
  module.exports = app;
})();

var server = new global.server(imports);
server.listen(4321);