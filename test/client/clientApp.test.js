describe('server request', function() {
  var server;
  var clientApp;

  beforeEach(function() {
    server = sinon.fakeServer.create();
    clientApp = new myApp.client({});
  });

  afterEach(function() {
    server.restore();
  });

  describe('ajax method', function() {
    it('should respond with GET', function(done) {
      clientApp.ajax({
        method: 'GET',
        url: '/user/' + 1
      }, function(data) {
      });

      done();
    });
  });

  describe('getUserTimeZone method', function() {
    it('should respond with GET', function(done) {
      clientApp.getUserTimeZone(1, function(data) {
      });

      expect(server.requests[0].method).to.equal('GET');
      done();
    });
  });

  describe('getCurrentTimeAjax method', function() {
    it('should respond with GET', function(done) {
      clientApp.getCurrentTimeAjax(1, function(data) {
      });

      expect(server.requests[0].method).to.equal('GET');
      done();
    });
  });
});

describe('getTwoDigits method', function() {
  it('1 should respond with 01', function(done) {
    var clientApp = new myApp.client({});

    var res = clientApp.getTwoDigits(1);

    expect(res).to.equal('01');
    done();
  });

  it('11 should respond with 11', function(done) {
    var clientApp = new myApp.client({});

    var res = clientApp.getTwoDigits(11);

    expect(res).to.equal('11');
    done();
  });
});