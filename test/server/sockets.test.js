var request = require('supertest');
var expect = require('chai').expect;
var should = require('chai').should();

var imports = {
  express: require('express'),
  http: require('http'),
  socketio: require('socket.io'),
  UserRouters: require('../../lib/users/routers/user.routers'),
  CurrentTimeRouters: require('../../lib/users/routers/currentTime.routers'),
  CurrentTimeSockets: require('../../lib/users/sockets/currentTime.sockets')
};

var app = require('../../main.js');
var server = new app(imports);

var io = require('socket.io-client');

describe('SOCKETS getCurrentTime(1)', function() {
  beforeEach(function() {
    this.socket = io.connect('http://localhost:4321');
  });

  afterEach(function() {
    this.socket.emit('stopCurrentTime');
  });

  it('should return originalServerTime + 1 hour', function(done) {
    this.socket.emit('stopCurrentTime');
    this.socket.emit('getCurrentTime', 1);

    this.socket.on('time', function(data) {
      data.should.have.property('serverTime');

      var timeOriginalServerTime = new Date(data.originalServerTime);
      timeOriginalServerTime.setHours(timeOriginalServerTime.getHours() + 1);

      expect(data.serverTime).to.equal(timeOriginalServerTime.toString());

      done();
    });
  });
});