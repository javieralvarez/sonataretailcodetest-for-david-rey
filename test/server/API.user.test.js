var request = require('supertest');
var expect = require('chai').expect;
var should = require('chai').should();

var imports = {
  express: require('express'),
  http: require('http'),
  socketio: require('socket.io'),
  UserRouters: require('../../lib/users/routers/user.routers'),
  CurrentTimeRouters: require('../../lib/users/routers/currentTime.routers'),
  CurrentTimeSockets: require('../../lib/users/sockets/currentTime.sockets')
};

var app = require('../../main.js');
var server = new app(imports);

describe('GET /user/:userId', function() {
  it('should respond with json', function(done) {
    request(server.express)
    .get('/user/9')
    .expect('Content-Type', /text\/html/)
    .expect(200, done);
  });

  it('/user/8 should equal to -2', function(done) {
    request(server.express)
    .get('/user/8')
    .end(function(err, res){
      if (err) {
        return done(err);
      }

      expect(res.text).to.equal('-2');
      done();
    });
  });

  it('/user/7 should equal to -3', function(done) {
    request(server.express)
    .get('/user/7')
    .end(function(err, res){
      if (err) {
        return done(err);
      }

      expect(res.text).to.equal('-3');
      done();
    });
  });
});