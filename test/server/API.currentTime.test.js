var request = require('supertest');
var expect = require('chai').expect;
var should = require('chai').should();

var imports = {
  express: require('express'),
  http: require('http'),
  socketio: require('socket.io'),
  UserRouters: require('../../lib/users/routers/user.routers'),
  CurrentTimeRouters: require('../../lib/users/routers/currentTime.routers'),
  CurrentTimeSockets: require('../../lib/users/sockets/currentTime.sockets')
};

var app = require('../../main.js');
var server = new app(imports);

describe('GET /currentTime/:timeZone', function() {
  it('should respond with json', function(done) {
    request(server.express)
    .get('/currentTime/3')
    .expect('Content-Type', /json/)
    .expect(200, done);
  });

  it('should have property serverTime', function(done) {
    request(server.express)
    .get('/currentTime/3')
    .end(function(err, res){
      if (err) {
        return done(err);
      }

      res.body.should.have.property('serverTime');
      done();
    });
  });

  it('should return originalServerTime + 3 hour', function(done) {
    request(server.express)
    .get('/currentTime/3')
    .end(function(err, res){
      if (err) {
        return done(err);
      }

      res.body.should.have.property('originalServerTime');

      var timeOriginalServerTime = new Date(res.body.originalServerTime);
      timeOriginalServerTime.setHours(timeOriginalServerTime.getHours() + 3);

      expect(res.body.serverTime).to.equal(timeOriginalServerTime.toString());

      done();
    });
  });
});